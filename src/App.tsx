import React, { useState } from 'react';
import './App.scss';
import { Todo, AddTodo } from './component/types';
import { TodoList } from './component/Todo/TodoList';
import { TodoItemForm } from './component/Todo/TodoForm/TodoItemForm';

const initialState: Todo[] = [
  { text: "Walk the dog", completed: true },
  { text: "Write app", completed: false }
]
function App() {

  const [todos, setTodos] = useState(initialState);

  const toggleTodo = (selectedTodo: Todo) => {
    const newTodo = todos.map(todo => {
      if (todo === selectedTodo) {
        return {
          ...todo,
          completed: !todo.completed
        }
      }
      return todo;
    });
    setTodos(newTodo);
  }

  const addTodo: AddTodo = (newTodo) => {
    newTodo.trim() !== "" &&
      setTodos([...todos, { text: newTodo, completed: false }])
  }

  return (
    <React.Fragment>
      <TodoList todos={todos} toggleTodo={toggleTodo} />
      <TodoItemForm addTodo={addTodo} />
    </React.Fragment>
  );
}

export default App;
