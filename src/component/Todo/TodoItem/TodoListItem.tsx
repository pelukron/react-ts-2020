import React from 'react'
import { Todo } from '../../types'
import './TodoListItem.scss';

interface TodoListItemProps {
    todo: Todo;
    toggleTodoInterface: (selectedTodo: Todo) => void
}

export const TodoListItem: React.FC<TodoListItemProps> = ({ todo, toggleTodoInterface }) => {
    return (
        <li>
            <label className={todo.completed ? "complete" : undefined}></label>
            <input type="checkbox" checked={todo.completed} onChange={() => toggleTodoInterface(todo)} />
            {todo.text}
        </li>
    );
}
