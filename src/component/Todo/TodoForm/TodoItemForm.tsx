import React, { useState, ChangeEvent, FormEvent } from 'react'
import { AddTodo } from '../../types';

interface TodoItemFormProps {
    addTodo: AddTodo
}
export const TodoItemForm: React.FC<TodoItemFormProps> = ({ addTodo }) => {

    // const [newTodo, setNewTodo] = Re
    const [newTodo, setNewTodo] = useState<string>("");

    const handleChange = (e: ChangeEvent<HTMLInputElement>) => {
        setNewTodo(e.target.value)
    }

    const handleSummit = (e: FormEvent<HTMLButtonElement>) => {
        e.preventDefault();
        addTodo(newTodo);
        setNewTodo('');
    }

    return (
        <div>
            <input type="text" value={newTodo} onChange={handleChange} />
            <button type="submit" onClick={handleSummit}>Add Todo</button>
        </div>
    )
}
