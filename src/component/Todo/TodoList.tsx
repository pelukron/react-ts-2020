import React from 'react'
import { TodoListItem } from './TodoItem/TodoListItem';
import { Todo } from '../types';

interface TodoListProps {
    todos: Todo[];
    toggleTodo: (selectedTodo: Todo) => void
}

export const TodoList: React.FC<TodoListProps> = ({ todos, toggleTodo }) => {

    return (
        <div>
            {todos.map(todo => {
                return <TodoListItem key={todo.text} todo={todo} toggleTodoInterface={toggleTodo} />
            })}
        </div>
    );

}
