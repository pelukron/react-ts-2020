export type Todo = {
    text: string,
    completed: boolean
};

export type AddTodo = (text: string) => void